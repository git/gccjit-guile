;; Copyright (C) 2019 Marc Nieper-Wißkirchen

;; This file is part of gccjit-Guile.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (gccjit)
	     (srfi srfi-11)
	     (srfi srfi-28))

(define (make-main context)
  (let* ((int-type
	  (jit-context-type-int context))
	 (param-argc
	  (jit-context-make-param context #f int-type "argc"))
	 (char-ptr-ptr-type
	  (jit-type-pointer (jit-type-pointer (jit-context-type-char context))))
	 (param-argv
	  (jit-context-make-param context #f char-ptr-ptr-type "argv"))
	 (params
	  (vector param-argc param-argv))
	 (func-main
	  (jit-context-make-function-exported context
					      #f
					      int-type
					      "main"
					      params
					      #f)))
    func-main))

(define (bf-compile filename)
  (call-with-input-file filename
    (lambda (port)
      (let* ((context (jit-make-context))
	     (void-type (jit-context-type-void context))
	     (int-type (jit-context-type-int context))
	     (byte-type (jit-context-type-unsigned-char context))
	     (array-type (jit-context-make-array-type context
						      #f
						      byte-type
						      30000))
	     (func-getchar (jit-context-make-function-imported context
							       #f
							       int-type
							       "getchar"
							       #()
							       #f))
	     (param-c (jit-context-make-param context #f int-type "c"))
	     (func-putchar (jit-context-make-function-imported context
							       #f
							       void-type
							       "putchar"
							       (vector param-c)
							       #f))
	     (func (make-main context))
	     (curblock (jit-function-add-block! func "initial"))
	     (int-zero (jit-context-zero context int-type))
	     (int-one (jit-context-one context int-type))
	     (byte-zero (jit-context-zero context byte-type))
	     (byte-one (jit-context-one context byte-type))
	     (data-cells (jit-context-make-global-internal context
							   #f
							   array-type
							   "data_cells"))
	     (idx (jit-function-add-local! func #f int-type "idx")))
	(define (fatal-error line col msg)
	  (display (format "~a:~a:~a: ~a~%" filename line col msg)
		   (current-error-port))
	  (exit 1))
	(define (current-data loc)
	  (jit-context-make-array-access context
					 loc
					 (jit-lvalue->rvalue data-cells)
					 (jit-lvalue->rvalue idx)))
	(define (current-data-zero? loc)
	  (jit-context-make-comparison-eq context
					  loc
					  (jit-lvalue->rvalue
					   (current-data loc))
					  byte-zero))
	(define (bf-compile-char line col char curblock tests bodies afters)
	  (define (return)
	    (values curblock tests bodies afters))
	  (let ((loc (jit-context-make-location context filename line col)))
	    (case char
	      ((#\>)
	       (jit-block-add-comment! curblock loc "'>': idx += 1;")
	       (jit-block-add-assignment-plus! curblock
						  loc
						  idx
						  int-one)
	       (return))
	      ((#\<)
	       (jit-block-add-comment! curblock loc "'<': idx -= 1;")
	       (jit-block-add-assignment-minus! curblock
						   loc
						   idx
						   int-one)
	       (return))
	      ((#\+)
	       (jit-block-add-comment! curblock loc "'+': data[idx] += 1;")
	       (jit-block-add-assignment-plus! curblock
						  loc
						  (current-data loc)
						  byte-one)
	       (return))
	      ((#\-)
	       (jit-block-add-comment! curblock loc "'-': data[idx] -= 1;")
	       (jit-block-add-assignment-minus! curblock
						loc
						(current-data loc)
						byte-one)
	       (return))
	      ((#\.)
	       (let* ((arg (jit-context-make-cast context
						  loc
						  (jit-lvalue->rvalue
						   (current-data loc))
						  int-type))
		      (call (jit-context-make-call context
						   loc
						   func-putchar
						   (vector arg))))
		 (jit-block-add-comment! curblock
					 loc
					 "'.': putchar ((int) data[idx]);")
		 (jit-block-add-eval! curblock loc call))
	       (return))
	      ((#\,)
	       (let ((call (jit-context-make-call context
						  loc
						  func-getchar
						  #())))
		 (jit-block-add-comment!
		  curblock
		  loc
		  "',': data[idx] = (unsigned char) getchar ();")
		 (jit-block-add-assignment! curblock
					    loc
					    (current-data loc)
					    (jit-context-make-cast
					     context loc call byte-type)))
	       (return))
	      ((#\[)
	       (let* ((loop-test (jit-function-add-block! func #f))
		      (on-zero (jit-function-add-block! func #f))
		      (on-not-zero (jit-function-add-block! func #f)))
		 (jit-block-end-with-jump! curblock loc loop-test)
		 (jit-block-add-comment! loop-test loc "'['")
		 (jit-block-end-with-conditional! loop-test
						  loc
						  (current-data-zero? loc)
						  on-zero
						  on-not-zero)
		 (values on-not-zero
			 (cons loop-test tests)
			 (cons on-not-zero bodies)
			 (cons on-zero afters))))
	      ((#\])
	       (jit-block-add-comment! curblock loc "']'")
	       (when (null? tests)
		 (fatal-error line col "mismatching parens"))
	       (jit-block-end-with-jump! curblock
					 loc
					 (car tests))
	       (values (car afters)
		       (cdr tests)
		       (cdr bodies)
		       (cdr afters)))
	      (else
	       (return)))))
	(jit-context-set-optimization-level! context 3)
	(jit-context-set-debug-info?! context #t)
	(jit-block-add-comment! curblock #f "idx = 0;")
	(jit-block-add-assignment! curblock #f idx int-zero)
	(let loop ((line 1) (col 0)
		   (curblock curblock) (tests '()) (bodies '()) (afters '()))
	  (let ((char (read-char port)))
	    (if (not (eof-object? char))
		(let-values (((curblock tests bodies afters)
			      (bf-compile-char line col char
					       curblock tests bodies afters)))
		  (if (char=? char #\newline)
		      (loop (+ line 1) 0 curblock tests bodies afters)
		      (loop line (+ col 1) curblock tests bodies afters)))
		(jit-block-end-with-return! curblock #f int-zero))))
	context))))

(define (main args)
  (unless (= (length args) 3)
    (display (format "~a: [INPUT FILE] [OUTPUT FILE]~%" (list-ref args 0)))
    (exit 1))
  (let ((input-file (list-ref args 1))
	(output-file (list-ref args 2)))
    (catch 'gccjit
      (lambda ()
	(let ((context (bf-compile input-file)))
	  (jit-context-compile-to-executable context output-file)))
      (lambda (key . args)
	(exit 1)))))
