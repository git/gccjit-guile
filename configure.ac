#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

# Copyright (C) 2019  Marc Nieper-Wißkirchen

# This file is part of gccjit-Guile, Guile bindings for libgccjit.

# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

AC_PREREQ([2.69])
AC_INIT([gccjit-Guile],
        m4_esyscmd([build-aux/git-version-gen .tarball-version]),
        [gccjit-guile-bug@nongnu.org],
        [],
	[http://www.nongnu.org/gccjit-guile/])
AC_CONFIG_AUX_DIR([build-aux])
AC_CONFIG_MACRO_DIR([m4])
AC_CONFIG_SRCDIR([src/gccjit.c])
AC_CONFIG_HEADERS([config.h])
AC_REQUIRE_AUX_FILE([test-driver.scm])
AM_INIT_AUTOMAKE([1.16])
AM_SILENT_RULES([yes])

# Checks for programs.
AC_PROG_CC
gl_EARLY

AM_PROG_AR
LT_PREREQ([2.4.2])
LT_INIT([dlopen])

GUILE_PKG
GUILE_FLAGS
GUILE_PROGS
if test -z "$GUILD"; then
  AC_PATH_PROG(GUILD, [guild])
fi

# For gnulib.
gl_INIT

# Checks for libraries.
AC_CHECK_LIB([gccjit], [gcc_jit_context_acquire])

# Checks for header files.

# Checks for typedefs, structures, and compiler characteristics.

# Checks for library functions.

AC_CONFIG_FILES([Makefile
                 lib/Makefile
                 include/Makefile
		 src/Makefile
		 doc/Makefile
		 tests/Makefile])
AC_OUTPUT
