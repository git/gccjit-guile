;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

;; Copyright (C) 2019 Marc Nieper-Wißkirchen

;; This file is part of gccjit-Guile, Guile bindings for libgccjit.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

((scheme-mode
  (indent-tabs-mode)
  (eval progn
	(put 'jit-call-with-result 'scheme-indent-function 1)
	(font-lock-add-keywords
	 nil
	 '()))))
